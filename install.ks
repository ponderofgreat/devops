# Configure installation method	
url --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch"

# zerombr
zerombr

# Configure Boot Loader
bootloader --location=mbr --driveorder=sda

# Create Physical Partition
part /boot --size=512 --asprimary --ondrive=sda --fstype=xfs
part swap --size=10240 --ondrive=sda 
part / --size=8192 --grow --asprimary --ondrive=sda --fstype=xfs

# Remove all existing partitions
clearpart --all --initlabel

# Configure Firewall
firewall --enabled --ssh

# Configure Network Interfaces
network --onboot=yes --bootproto=dhcp --hostname=ponderofgreat

# Configure Keyboard Layouts
keyboard ru

# Configure Language During Installation
lang en_US

# Configure X Window System
xconfig --startxonboot

# Configure Time Zone
timezone Europe/Saratov

# Create User Account
user --name=ponderofgreat --plaintext --password=Qwerty_12 --groups=wheel

# Set Root Password
rootpw --lock

# Perform Installation in Text Mode
text

# Package Selection
%packages

chromium
java-openjdk
@Python Classroom
firefox
@LibreOffice
@gnome-desktop
ansible
git
vim
docker
wget

%end

# Post-installation Script
%post
systemctl enable docker.service
systemctl start docker.service
curl -o /usr/bin/docker_start.sh https://bitbucket.org/ponderofgreat/devops/raw/ec8e71988dab5b9faee2d08c8c5f260ce7ea69eb/docker_start.sh
chmod +x /usr/bin/docker_start.sh
curl -o /etc/systemd/system/docker_start.service https://bitbucket.org/ponderofgreat/devops/raw/ec8e71988dab5b9faee2d08c8c5f260ce7ea69eb/docker_start.service
chmod 644 /etc/systemd/system/docker_start.service
systemctl enable docker_start.service
%end

# Reboot After Installation
reboot --eject
